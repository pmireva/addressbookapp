﻿using AddressBook.Data.Address.DataObjects;
using AddressBook.Data.Infrastructure;

namespace AddressBook.Data.Address
{
    public interface IAddressRepository : IRepository<Address>
    {
        PagedResult<AddressDto> Get(int page, int limit);
    }
}
