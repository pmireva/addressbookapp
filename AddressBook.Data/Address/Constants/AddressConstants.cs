﻿namespace AddressBook.Data.Address.Constants
{
    public static class AddressConstants
    {
        public static string AddressDataIsMissing = "Address data is missing";
        public static string ThereIsNoChange = "No change has been made";
        public static string GuidIsInvalid = "Invalid Id of Address";
    }
}
