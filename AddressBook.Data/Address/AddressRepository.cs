﻿using System.Collections.Generic;
using System.Linq;
using AddressBook.Data.Address.DataObjects;
using AddressBook.Data.Infrastructure;
using AddressBook.Data.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace AddressBook.Data.Address
{
    public class AddressRepository : Repository<Address>, IAddressRepository, IIndexedRepository
    {
        public AddressRepository(IOptions<Settings> settings) : base(settings)
        {

        }

        public void EnsureIndexes()
        {
        }

        public PagedResult<AddressDto> Get(int page, int limit)
        {
            var query = this.dbContext.Set<Address>().Find(a => true);
            int total = (int)query.CountDocuments();
            var addresses = query
                .Skip((page - 1) * limit)
                .Limit(limit)
                .Project(Address.Selector)
                .ToList();
            return new PagedResult<AddressDto>
            {
                Items = addresses,
                TotalCount = total
            };
        }
    }
}
