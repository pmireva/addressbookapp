﻿using AddressBook.Data.Infrastructure.Models;
using System;

namespace AddressBook.Data.Address.DataObjects
{
    public class AddressDto : TDataObject
    {
        public string id { get; set; }
        public string Address { get; set; }
        public string PersonNames { get; set; }
    }
}
