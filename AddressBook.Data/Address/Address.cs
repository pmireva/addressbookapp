﻿using AddressBook.Data.Address.DataObjects;
using AddressBook.Data.Models;
using System;
using System.Linq.Expressions;

namespace AddressBook.Data.Address
{
    public class Address : TEntity
    {
        public string PersonNames { get; private set; }
        public string AddressDescription { get; private set; }

        public Address()
        {
        }

        public Address(string names, string address)
        {
            this.PersonNames = names;
            this.AddressDescription = address;
        }

        public Address(AddressDto data)
        {
            this.PersonNames = data.PersonNames;
            this.AddressDescription = data.Address;
        }

        public Address Update(AddressDto data)
        {
            this.PersonNames = data.PersonNames ?? this.PersonNames;
            this.AddressDescription = data.Address ?? this.AddressDescription;

            return this;
        }

        public static Expression<Func<Address, AddressDto>> Selector
        {
            get
            {
                return x => new AddressDto
                {
                    PersonNames = x.PersonNames,
                    Address = x.AddressDescription,
                    id = x.Id.ToString()
                };
            }
        }

        public static Func<Address, AddressDto> CompiledSelector = Selector.Compile();
    }
}
