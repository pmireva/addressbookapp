﻿using AddressBook.Data.Infrastructure;
using AddressBook.Data.Models;
using MongoDB.Driver;
using System;
using System.Linq.Expressions;

namespace AddressBook.Data
{
    public interface IRepository<T> where T : TEntity
    {
        T Get(Guid id);
        T Add(T item);
        ReplaceOneResult Update(T item);
        T Remove(Guid id);
        DeleteResult RemoveAll();
    }
}
