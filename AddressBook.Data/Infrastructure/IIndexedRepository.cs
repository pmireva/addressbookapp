﻿namespace AddressBook.Data.Infrastructure
{
    public interface IIndexedRepository
    {
        void EnsureIndexes();
    }
}
