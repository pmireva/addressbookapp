﻿using AddressBook.Data.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;

namespace AddressBook.Data
{
    public class DbContext
    {
        private readonly IMongoDatabase _database;

        public DbContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);

            var conventionPack = new ConventionPack { new IgnoreExtraElementsConvention(true) };
            ConventionRegistry.Register("IgnoreExtraElements", conventionPack, type => true);
        }

        public IMongoCollection<T> Set<T>()
        {
            return _database.GetCollection<T>(typeof(T).Name);
        }
    }
}
