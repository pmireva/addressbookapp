﻿using System.Collections.Generic;

namespace AddressBook.Data.Infrastructure
{
    public class PagedResult<T>
    {
        public int TotalCount { get; set; }
        public IList<T> Items { get; set; }
    }
}
