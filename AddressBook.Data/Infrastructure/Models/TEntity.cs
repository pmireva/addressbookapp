﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace AddressBook.Data.Models
{
    public class TEntity
    {
        [BsonId]
        public Guid Id { get; set; }
    }
}
