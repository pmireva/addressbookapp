﻿using AddressBook.Data.Address;
using AddressBook.Data.Address.Constants;
using AddressBook.Data.Address.DataObjects;
using AddressBook.Logic;
using AddressBook.Logic.Address.Validators;
using AddressBook.Logic.Infrastructure.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddressBook.UnitTests
{
    [TestFixture]
    public class AddressMediatorTests
    {
        private Mock<IAddressRepository> addressRepository;
        private Mock<IAddressValidator> validator;

        private IAddressMediator mediator;

        private static string errorMessage = "testError";
        private static ValidationResponse validationResponse = new ValidationResponse
        {
            Errors = new List<string> { errorMessage }
        };

        [SetUp]
        public void Setup()
        {
            this.addressRepository = new Mock<IAddressRepository>();
            this.validator = new Mock<IAddressValidator>();
            this.mediator = new AddressMediator(
                this.addressRepository.Object,
                this.validator.Object);

        }

        #region Create
        [TestCase]
        public void Create_InvalidDataNullPassed_CorrectResponseWithError()
        {
            validator.Setup(v => v.Validate(null))
                .Returns(validationResponse);

            var result = this.mediator.Create(null);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Validation);
            Assert.IsFalse(result.Validation.IsValid);

            Assert.IsNull(result.CreatedItem);

            Assert.AreEqual(1, result.Validation.Errors.Count());
            Assert.AreEqual(errorMessage, result.Validation.Errors.First());
        }

        [TestCase]

        public void Create_InvalidDataEmptyFields_CorrectResponseWithError()
        {
            var testData = new AddressDto();
            validator.Setup(v => v.Validate(It.Is<AddressDto>(a => a == testData)))
                .Returns(validationResponse);

            var result = this.mediator.Create(testData);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Validation);
            Assert.IsFalse(result.Validation.IsValid);

            Assert.IsNull(result.CreatedItem);

            Assert.AreEqual(1, result.Validation.Errors.Count());
            Assert.AreEqual(errorMessage, result.Validation.Errors.First());
        }

        [TestCase]

        public void Create_ValidData_CorrectResponse()
        {
            var newEntry = new AddressDto
            {
                PersonNames = "Test",
                Address = "AddressTest"
            };
            var testData = new AddressDto();
            validator.Setup(v => v.Validate(It.Is<AddressDto>(a => a == newEntry)))
                .Returns(new ValidationResponse());

            var result = this.mediator.Create(newEntry);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Validation);
            Assert.AreEqual(0, result.Validation.Errors.Count);

            Assert.IsNotNull(result.CreatedItem);

            Assert.AreEqual(newEntry.PersonNames, result.CreatedItem.PersonNames);
            Assert.AreEqual(newEntry.Address, result.CreatedItem.Address);
        }
        #endregion

        #region Update
        [TestCase]

        public void Update_ValidData_CorrectResponse()
        { var guid = Guid.NewGuid();
            string expectedAddress = "AddressTest";
            string expectedPersonNames = "Test";
            var updateEntry = new AddressDto
            {
                id = guid.ToString(),
                PersonNames = expectedPersonNames
            };
            var testData = new AddressDto();
            validator.Setup(v => v.Validate(It.Is<AddressDto>(a => a == updateEntry)))
                .Returns(new ValidationResponse());

            var entry = addressRepository.Setup(r => r.Get(guid))
                .Returns(new Address("PreviousName", expectedAddress));

            var result = this.mediator.Update(updateEntry.id, updateEntry);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Validation);
            Assert.AreEqual(0, result.Validation.Errors.Count);

            Assert.IsNotNull(result.CreatedItem);

            Assert.AreEqual(expectedPersonNames, result.CreatedItem.PersonNames);
            Assert.AreEqual(expectedAddress, result.CreatedItem.Address);
        }

        [TestCase]

        public void Update_InvalidData_CorrectResponse()
        {
            var guid = Guid.NewGuid();
            var updateEntry = new AddressDto();
            var testData = new AddressDto();
            validator.Setup(v => v.Validate(It.Is<AddressDto>(a => a == updateEntry)))
                .Returns(validationResponse);

            var entry = addressRepository.Setup(r => r.Get(guid))
                .Returns(new Address("PreviousName", "Test"));

            var result = this.mediator.Update(guid.ToString(), updateEntry);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Validation);
            Assert.AreEqual(1, result.Validation.Errors.Count);
            Assert.AreEqual(errorMessage, result.Validation.Errors.First());

            Assert.IsNull(result.CreatedItem);
        }

        [TestCase]

        public void Update_InvalidGuid_CorrectResponse()
        {
            var guid = Guid.NewGuid();
            var updateEntry = new AddressDto
            {
                PersonNames = "Test"
            };
            var testData = new AddressDto();
            validator.Setup(v => v.Validate(It.Is<AddressDto>(a => a == updateEntry)))
                .Returns(validationResponse);

            var entry = addressRepository.Setup(r => r.Get(guid))
                .Returns(new Address("PreviousName", "Test"));

            var result = this.mediator.Update(updateEntry.id, updateEntry);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Validation);
            Assert.AreEqual(1, result.Validation.Errors.Count);
            Assert.AreEqual(AddressConstants.GuidIsInvalid, result.Validation.Errors.First());

            Assert.IsNull(result.CreatedItem);
        }
        #endregion
    }
}
