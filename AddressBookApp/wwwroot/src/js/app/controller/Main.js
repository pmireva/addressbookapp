Ext.define("App.controller.Main", {
	extend: "Ext.app.Controller",
	alias : "widget.Main",
	views: [
		"App.view.PeopleList"
	]
});
