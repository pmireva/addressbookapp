Ext.define("App.view.PeopleList", {
	extend: "Ext.grid.Panel",
	xtype: "peoplelist",
	width: 350,
	height: 400,
	requires: [
		"App.store.Person"
	],
	store: "Person",
	title: "Address Book",
	selModel: "rowmodel",
	plugins: {
		ptype: "rowediting",
		clicksToEdit: 1
	},
	dockedItems: [{
		xtype: "pagingtoolbar",
		dock: "bottom",
		store: "Person",
		id: 'paginator'
	}],
	columns: [{
		text: "Person Names",
		dataIndex: "PersonNames",
		name: "personNames",
		sortable: false,
		editor: {
			xtype: "textfield",
			allowBlank: false
		}
	}, {
		text: "Address",
		dataIndex: "Address",
		name: "address",
		sortable: false,
		editor: {
			xtype: "textfield",
			allowBlank: false
		}
	}, {
		xtype: "actioncolumn",
		text: "Delete",
		width: 50,
		align: "center",
		items: [{
			getClass: function () {
				return "x-toast-icon-error";
			},
			handler: function (grid, rowIndex) {
				var store = Ext.getStore("Person");
				var item = store.getAt(rowIndex);
				store.removeAt(rowIndex);
				store.erase(item.data["Id"]);
				store.load();
			}
		}]
	}],
	tbar: [{
			text: "Add Contact",
			handler: function () {
				var person = { PersonNames: "New Name", Address: "New Address" };
				var store = Ext.getStore("Person");
				store.insert(0, person);
				store.sync();
				store.load();
				var paginator = Ext.getCmp('paginator');
				paginator.moveLast();
			}
		}, {
			text: "Save All Changes",
			handler: function () {
				var store = Ext.getStore("Person");
				store.sync();
			}
		}, {
			text: "Cancel All Changes",
			handler: function () {
				var store = Ext.getStore("Person");
				store.load();
			}
		}]
});
