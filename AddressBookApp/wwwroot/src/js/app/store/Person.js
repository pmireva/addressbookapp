Ext.define("App.store.Person", {
	extend: "Ext.data.Store",
	autoLoad: true,
	alias: "store.Person",
	idProperty: "id",
	pageSize: 10,
	fields: [
		{
			name: "id",
			type: "string",
			allowNull: true
		},
		{
			name: "PersonNames",
			type: "string"
		},
		{
			name: "Address",
			type: "string"
		}
	],
	proxy: {
		type: "rest",
		url: "api/addresses",
		reader: {
			type: "json",
			rootProperty: "Items",
			totalProperty: "TotalCount"
		},
		writer: {
			type: "json"
		}
	},
	autoSync: false,
	autoSave: false,
	remoteSort: false
});
