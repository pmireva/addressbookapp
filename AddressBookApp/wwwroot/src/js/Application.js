Ext.application({
	name: "App",
	controllers: [
		"Main"
	],
	requires: [
		"App.view.PeopleList"
	],
	stores: ["Person"],
	launch: function () {
		Ext.create("Ext.container.Viewport", {
			layout: "border",
			margin: 25,
			items: [
				{
					xtype: "peoplelist"
				}
			]
		});
	}
});
