"use strict";

const $ = require("../../config.js");
const imagesSrc = $.config.srcImageLibs;
const destination = $.config.dest;

const ImageFnc = function() {
	return $.gulp.src(imagesSrc, { base: "src" })
		.pipe($.gulp.dest(destination));
};

$.gulp.task(
	"images",
	`Copies and minimizes any images from the ${imagesSrc} folder to the ${destination} folder`,
	[],
	ImageFnc
);
