﻿using AddressBook.Data.Address;
using AddressBook.Data.Address.DataObjects;
using AddressBook.Data.Infrastructure;
using AddressBook.Logic;
using AddressBook.Logic.Address.DataObjects;
using Microsoft.AspNetCore.Mvc;
using System;

namespace AddressBookApp.Controllers
{
    [Route("api/addresses")]
    public class AddressesController : Controller
    {
        private readonly IAddressRepository addressRepository;
        private readonly IAddressMediator addressMediator;

        public AddressesController(
            IAddressRepository addressRepository,
            IAddressMediator addressMediator)
        {
            this.addressRepository = addressRepository;
            this.addressMediator = addressMediator;
        }

        [HttpGet]
        public PagedResult<AddressDto> All(int page, int limit)
        {
            var result = addressRepository.Get(page, limit);
            return result;
        }

        [HttpPost]
        public CreateAddressDto CreateNew([FromBody]AddressDto data)
        {
            return addressMediator.Create(data);
        }
        
        [HttpPut("{id}")]
        public UpdateAddressDto Update(string id, [FromBody]AddressDto data)
        {
            return addressMediator.Update(id, data);
        }

        [HttpDelete("{Id}")]
        public OkResult Delete(string Id)
        {
            addressRepository.Remove(Guid.Parse(Id));

            return Ok();
        }
    }
}