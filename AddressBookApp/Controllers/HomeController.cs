﻿using Microsoft.AspNetCore.Mvc;

namespace AddressBookApp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
