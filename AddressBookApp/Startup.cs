﻿using AddressBook.Data.Address;
using AddressBook.Logic.Address.Validators;
using AddressBook.Data.Infrastructure;
using AddressBook.Data.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using System.IO;
using AddressBook.Logic;

namespace AddressBookApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());
            string connectionString = Configuration.GetSection("MongoConnection:ConnectionString").Value;
            string database = Configuration.GetSection("MongoConnection:Database").Value;

            services.Configure<Settings>(options =>
            {
                options.ConnectionString = connectionString;
                options.Database = database;
            });
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IAddressValidator, AddressValidator>();
            services.AddTransient<IAddressMediator, AddressMediator>();
            services.AddTransient<IAddressRepository, AddressRepository>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var settings = serviceScope.ServiceProvider.GetService<IOptions<Settings>>();
                    var repositories = serviceScope.ServiceProvider.GetServices<IIndexedRepository>();
                    foreach (var repo in repositories)
                    {
                        repo.EnsureIndexes();
                    }
                }
            }

            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory())),
                RequestPath = new PathString("/build/images")
            });
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
