﻿using AddressBook.Data.Infrastructure.Models;
using AddressBook.Logic.Infrastructure.Models;

namespace AddressBook.Logic.Infrastructure
{
    public class CreateUpdateResponseDto<T> where T : TDataObject
    {
        public CreateUpdateResponseDto(ValidationResponse validation, T item)
        {
            this.Validation = validation;
            this.CreatedItem = item;
        }
        public ValidationResponse Validation { get; set; }
        public T CreatedItem { get; set; }
    }
}
