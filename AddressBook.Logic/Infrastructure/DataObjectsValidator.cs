﻿using AddressBook.Data.Infrastructure.Models;
using AddressBook.Logic.Infrastructure.Models;

namespace AddressBook.Logic.Infrastructure
{
    public abstract class DataObjectsValidator<T> : IDataObjectsValidator<T> where T: TDataObject
    {
        public abstract ValidationResponse Validate(T data);
    }
}
