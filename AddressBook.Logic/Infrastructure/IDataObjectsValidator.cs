﻿using AddressBook.Data.Infrastructure.Models;
using AddressBook.Logic.Infrastructure.Models;

namespace AddressBook.Logic.Infrastructure
{
    public interface IDataObjectsValidator<T> where T : TDataObject
    {
        ValidationResponse Validate(T data);
    }
}