﻿using AddressBook.Data.Models;
using AddressBook.Logic.Infrastructure.Models;

namespace AddressBook.Data.Infrastructure.Models
{
    public class CreateUpdateResponse<T> where T : TEntity
    {
        public ValidationResponse Validation { get; set; }
        public T CreatedItem { get; set; }
    }
}
