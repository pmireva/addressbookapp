﻿using System.Collections.Generic;

namespace AddressBook.Logic.Infrastructure.Models
{
    public class ValidationResponse
    {
        public ValidationResponse()
        {
            this.Errors = new List<string>();
        }

        public ValidationResponse(List<string> errors) : this()
        {
            this.Errors = errors;
        }

        public bool IsValid
        {
            get
            {
                return this.Errors.Count == 0;
            }
        }

        public List<string> Errors { get; set; }
    }
}
