﻿using AddressBook.Data.Address.DataObjects;
using AddressBook.Logic.Address.DataObjects;

namespace AddressBook.Logic
{
    public interface IAddressMediator
    {
        CreateAddressDto Create(AddressDto data);
        UpdateAddressDto Update(string id, [FromBody] AddressDto data);
    }
}