﻿using AddressBook.Data.Address.DataObjects;
using AddressBook.Logic.Infrastructure;
using AddressBook.Logic.Infrastructure.Models;

namespace AddressBook.Logic.Address.DataObjects
{
    public class CreateAddressDto : CreateUpdateResponseDto<AddressDto>
    {
        public CreateAddressDto(ValidationResponse validation, AddressDto item) : base(validation, item)
        {
        }
    }
}
