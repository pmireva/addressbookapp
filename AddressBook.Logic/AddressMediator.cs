﻿using AddressBook.Data.Address;
using AddressBook.Data.Address.Constants;
using AddressBook.Data.Address.DataObjects;
using AddressBook.Logic.Address.DataObjects;
using AddressBook.Logic.Address.Validators;
using AddressBook.Logic.Infrastructure.Models;
using System;

namespace AddressBook.Logic
{
    public class AddressMediator : IAddressMediator
    {
        private readonly IAddressRepository addressRepository;
        private readonly IAddressValidator validator;
        public AddressMediator(
            IAddressRepository addressRepository,
            IAddressValidator validator)
        {
            this.validator = validator;
            this.addressRepository = addressRepository;
        }
        public CreateAddressDto Create(AddressDto data)
        {
            var validationResult = validator.Validate(data);
            var result = new CreateAddressResponse
            {
                Validation = validationResult
            };
            AddressDto newAddress = null;
            if (validationResult.IsValid)
            {
                var newAddressEntity = new Data.Address.Address(data);
                addressRepository.Add(newAddressEntity);
                result.CreatedItem = newAddressEntity;
                newAddress = Data.Address.Address.CompiledSelector(result.CreatedItem);
            }
           
            return new CreateAddressDto(result.Validation, newAddress);
        }

        public UpdateAddressDto Update(string id, [FromBody]AddressDto data)
        {
            var validationResponse = new ValidationResponse();
            Guid guid;
            if (!Guid.TryParse(id, out guid))
            {
                validationResponse.Errors.Add(AddressConstants.GuidIsInvalid);
                return new UpdateAddressDto(validationResponse, null);
            }
            var validationResult = validator.Validate(data);
            var result = new CreateAddressResponse
            {
                Validation = validationResult
            };

            var entry = addressRepository.Get(guid);
            var updatedEntry = entry.Update(data);

            AddressDto updatedAddress = null;
            if (validationResult.IsValid)
            {
                addressRepository.Update(entry);
                updatedAddress = Data.Address.Address.CompiledSelector(updatedEntry);
            }

            return new UpdateAddressDto(result.Validation, updatedAddress);
        }
    }
}
