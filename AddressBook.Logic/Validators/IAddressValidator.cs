﻿using AddressBook.Data.Address.DataObjects;
using AddressBook.Logic.Infrastructure;

namespace AddressBook.Logic.Address.Validators
{
    public interface IAddressValidator : IDataObjectsValidator<AddressDto>
    {
    }
}