﻿using AddressBook.Data.Address.Constants;
using AddressBook.Data.Address.DataObjects;
using AddressBook.Logic.Infrastructure;
using AddressBook.Logic.Infrastructure.Models;
using System.Collections.Generic;

namespace AddressBook.Logic.Address.Validators
{
    public class AddressValidator : DataObjectsValidator<AddressDto>, IAddressValidator
    {
        public override ValidationResponse Validate(AddressDto data)
        {
            var errors = new List<string>();
            if (data == null)
            {
                errors.Add(AddressConstants.AddressDataIsMissing);
            }
            if (data != null && 
                string.IsNullOrEmpty(data.Address) &&
                string.IsNullOrEmpty(data.PersonNames))
            {
                errors.Add(AddressConstants.ThereIsNoChange);
            }

            return new ValidationResponse(errors);
        }
    }
}
