﻿using AddressBook.Data.Infrastructure.Models;

namespace AddressBook.Logic.Address.DataObjects
{
    public class UpdateAddressResponse : CreateUpdateResponse<Data.Address.Address>
    {
    }
}
