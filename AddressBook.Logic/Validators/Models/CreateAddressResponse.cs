﻿using AddressBook.Data.Infrastructure.Models;

namespace AddressBook.Logic.Address.DataObjects
{
    public class CreateAddressResponse : CreateUpdateResponse<Data.Address.Address>
    {
    }
}
